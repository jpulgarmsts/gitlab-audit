# Gitlab Audit

* Generates an audit for all Gitlab commits for a particular Group ID and time span.
* You can run it for a whole organization or a particular team.
* Support auto-resume functionality if we get disconnected from Gitlab.

## Pre-requisites
* Create a Gitlab Personal Access Token with **read_api** and **read_repository** access
* Install python-gitlab library: `pip3 install python-gitlab`
* Install progress bar library: `pip3 install progress`

## Configuration
Add your necessary teams to config.py

## Required Parameters
* Group ID (could be the whole org or one team)
* Audit Start Date YYYY-MM-DD format
* Audit End Date YYYY-MM-DD format
* Gitlab Personal Access Token

## Output
* Gitlab audit stored within output/ folder

Whole Org Example:
* `python3 audit.py 4155577 2023-01-01 2023-03-31 GITLAB_TOKEN`

'The A-Team' Example:
* `python3 audit.py 9822702 2023-01-01 2023-03-31 GITLAB_TOKEN`

