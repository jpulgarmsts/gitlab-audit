import gitlab, argparse, configparser, time, os
from config import root_id
from progress.bar import Bar


# Define and parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("group_id", help="GitLab Group ID to start from")
parser.add_argument("start_date", help="Start Date YYYY-MM-DD")
parser.add_argument("end_date", help="End Date YYYY-MM-DD")
parser.add_argument("gitlab_token", help="Gitlab Access Token (read_api and read_repository access)")
args = parser.parse_args()
config = configparser.ConfigParser()

gl = gitlab.Gitlab(private_token=args.gitlab_token)

team_ids = []

def createResumeConfig(filename):
        file2 = open(filename, 'w')
        file2.write("[GROUPS]\n")
        file2.close()

def output_file(team_name, start_date, end_date, team_ids):

    file1 = open('output/' + team_name + ' ' + start_date + ' to ' + end_date + '.txt', 'w')

    # Report Heading
    file1.write(team_name + " Gitlab Audit Report\n")
    file1.write("Date Range: " + start_date + "T00:00:00Z" + " to " + end_date + "T23:59:59Z\n")
    file1.write('='*56)
    file1.write('\n')

    bar = Bar('Processing', max=len(team_ids))

    # Get Commit information for all the Projects
    for pid in team_ids:
        project = gl.projects.get(pid)

        file1.write("\n"+project.name + " (" + project.web_url + ")\n")
        file1.write(chr(175)*len(project.name + " (" + project.web_url + ")"))
        file1.write("\n")

        try:
            commits = project.commits.list(since=args.start_date+'T00:00:00Z', until=args.end_date+'T23:59:59Z', iterator=True)

            commitsFound = False
            for commit in commits:
                commitsFound = True
                file1.write("Commit: " + commit.title + "\n")
                file1.write("Author: " + commit.author_name + " (" + commit.author_email+ ")\n")
                file1.write("ID: " + commit.id + "\n")
                file1.write("Date: " + commit.committed_date + "\n")
                diff = commit.diff(iterator=True)
                for d in diff:
                    if(d['new_file']):
                        file1.write("\tAdded " + d['new_path'] + "\n")
                    elif d['deleted_file']:
                        file1.write("\tDeleted " + d['new_path'] + "\n")
                    elif d['renamed_file']:
                        file1.write("\tRenamed " + d['old_path'] + " to " + d['new_path'] + "\n")
                    else:
                        file1.write("\tModified " + d['new_path'] + "\n")
                file1.write("\n")
            if commitsFound == False:
                file1.write("No commits found for given time range.\n")

            # time.sleep(10)
        except:
            file1.write("Cannot access commits.\n")
        bar.next()
    file1.close()
    bar.finish()

# Iterate through all Group IDs that a team is responsible for
rootgroup = gl.groups.get(root_id)

# Get Root Level Misc Projects 
if args.group_id == str(root_id):
    if not os.path.exists("output/Misc Projects " + args.start_date + " to " + args.end_date + ".txt"):
        projects = rootgroup.projects.list(iterator=True)
        print(f"Root Level Misc Projects")
        for p in projects:
            print(f"     Project: {p.name}")
            team_ids.append(p.id)
        output_file("Misc Projects", args.start_date, args.end_date, team_ids)
        print(f"Generated: Misc Projects {args.start_date} to {args.end_date}.txt")

# Get Projects that are tied to Subgroups of that Group ID 
teamgroups = rootgroup.subgroups.list(iterator=True)

# Let's create a resume file if running company's root project
if args.group_id == str(root_id):
    
    if not os.path.exists(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume'):
        createResumeConfig(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume')
        config.read(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume')
        for c in teamgroups:
            config.set('GROUPS', str(c.id), c.name)
        with open(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume', 'w') as configfile:
            config.write(configfile)
        config.read(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume')
    else:
        print("Resuming from previous run...")
        config.read(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume')  
    
    groupsRemaining = list(config.items('GROUPS'))

else:
    for c in teamgroups:
        if args.group_id == str(c.id):
            groupsRemaining = [(str(c.id), c.name)]

current = 0

for s in groupsRemaining:

    if str(s[0]) == args.group_id or args.group_id == str(root_id):
        
        current = current + 1
        
        team_ids.clear()
        gg = gl.groups.get(s[0])
        
        print(f"\n{s[1]} {s[0]}")
        subprojects = gg.projects.list(iterator=True)
        
        # Team Root Projects
        for dg in subprojects:
            print(f"     Project: {dg.name}")
            team_ids.append(dg.id)
        
        # Team Subgroups
        teamsubgroups = gg.subgroups.list(iterator=True)
        for dg in teamsubgroups:
            print(f"     Subgroup: {dg.name}")

            # Team Subgroup Projects
            dgg = gl.groups.get(dg.id)
            teamsubprojects = dgg.projects.list(iterator=True)
            for mm in teamsubprojects:
                print(f"          Project: {mm.name}")
                team_ids.append(mm.id)

        output_file(s[1], args.start_date, args.end_date, team_ids)
        
        if args.group_id == str(root_id):
            print(f"Generated: {s[1]} {args.start_date} to {args.end_date}.txt [{current} of {len(groupsRemaining)}]")
            config.remove_option('GROUPS', str(s[0]))
            with open(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume', 'w') as configfile:
                config.write(configfile)
        else:
            print(f"Generated: {s[1]} {args.start_date} to {args.end_date}.txt")

# Delete the .resume file
if args.group_id == str(root_id):
    os.remove(args.group_id + '_' + args.start_date + '_' + args.end_date + '.resume')
        
